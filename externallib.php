<?php

// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * External Web Service Template
 *
 * @package    localwstemplate
 * @copyright  2011 Moodle Pty Ltd (http://moodle.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require_once($CFG->libdir . "/externallib.php");

class local_wsgricsquifacturer_external extends external_api {

    /**
     * Returns description of method parameters
     * @return external_function_parameters
     */
    public static function quifacturer_parameters() {
        return new external_function_parameters(
                array('welcomemessage' => new external_value(PARAM_TEXT, 'Bidon,"', VALUE_DEFAULT, 'Bidon, '))
        );
    }

    /**
     * Returns welcome message
     * @return string welcome message
     */
    public static function quifacturer($welcomemessage = 'Bidon, ') {
        global $USER;
        global $DB;

        //Parameter validation
        //REQUIRED
        $params = self::validate_parameters(self::quifacturer_parameters(),
                array('welcomemessage' => $welcomemessage));

        //Context validation
        //OPTIONAL but in most web service it should present
        $context = get_context_instance(CONTEXT_USER, $USER->id);
        self::validate_context($context);

        //Capability checking
        //OPTIONAL but in most web service it should present
        if (!has_capability('moodle/user:viewdetails', $context)) {
            throw new moodle_exception('cannotviewprofile');
        }

    $sql = "select a.comment as comment, b.userid as userid, c.courseid as courseid from {enrol_apply_applicationhist} a, {user_enrolments} b, {enrol} c where a.userenrolmentid=b.id and b.enrolid = c.id";

    $donnees = $DB->get_records_sql($sql,array());

	$result = array();

        foreach ($donnees as $donnee) {
            $resultdonnees = array(
                'userid' => $donnee->userid,
                'courseid' => $donnee->courseid,
                'comment' => $donnee->comment
            );
            $result[] = $resultdonnees;
		}

    return $donnees;
 }

    /**
     * Returns description of method result value
     * @return external_description
     */
    public static function quifacturer_returns() {
	return new external_multiple_structure(
		new external_single_structure(array(
                'userid' => new external_value(PARAM_INT, 'Is a id', VALUE_OPTIONAL),
                'courseid' => new external_value(PARAM_RAW, 'is enrolmentid', VALUE_OPTIONAL),
                'comment' => new external_value(PARAM_RAW, 'Is a comment', VALUE_OPTIONAL),
               )
)
        );
    }


}
